import coroutines.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlin.coroutines.coroutineContext
import kotlin.system.measureTimeMillis


fun main() = runBlocking {
    //single request
    val singleReq = PerformSingleNetworkRequest(mockApi = MockApi(), viewModelScope = CoroutineScope(coroutineContext))
    //2 sequential requests
    val twoSeqRequest = PerformTwoSequentialNetworkRequests(mockApi = MockApi(), viewModelScope = CoroutineScope(coroutineContext))
    //2 concurrent requests
    val twoConReq = PerformTwoConcurrentRequests(mockApi = MockApi(), viewModelScope = CoroutineScope(coroutineContext))

    val dynRequests = DynamicAmountOfRequests(mockApi = MockApi(), viewModelScope = CoroutineScope(coroutineContext))

//    println(singleReq.performSingleNetWorkRequest())
//
//    println(twoSeqRequest.performTwoSequentialNetworkRequest())
//
//    println(twoConReq.perfomTwoConcurrentRequests())

    println(dynRequests.dynamicAmountOfRequests())
}








