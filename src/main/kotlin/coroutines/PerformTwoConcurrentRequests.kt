package coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class PerformTwoConcurrentRequests(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun perfomTwoConcurrentRequests() {
        viewModelScope.launch {
            val userCall = async { mockApi.getUser() }
            val categoryCall = async { mockApi.getCategories() }
            val user = try {
                val user = userCall.await()
                val category = categoryCall.await()
            } catch (exception: Exception) {
                emptyList<String>()
            }

        }
    }

}