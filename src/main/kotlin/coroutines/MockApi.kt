package coroutines

import kotlinx.coroutines.delay

class MockApi {

    fun getUsers(): List<String>{
        println(listOf("gary", "rob", "flo"))
        return listOf("gary", "rob", "flo")
    }

    suspend fun getUser(): String{
        delay(1000)
        println("Davis")
        return "David"
    }

    suspend fun getCategories(num: Int = 3): Int{
        delay(1000)
        println(num)
        return num
    }

    suspend fun getCategoryList(categoryAmt: Int): MutableList<String>{
        delay(1000)
        var list = listOf("gary", "ricky", "bob", "britt", "mike", "brian", "jonas", "eliza")

        var chosenList: MutableList<String> = arrayListOf()

        for(i in 0 until categoryAmt){
            chosenList.add(list[i])
        }
        println(chosenList)
        return chosenList
    }

}