package coroutines

import kotlinx.coroutines.*


class PerformSingleNetworkRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {

    fun performSingleNetWorkRequest(){
        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
            }catch (exception: Exception){
                emptyList<String>()
            }
        }
    }

}

