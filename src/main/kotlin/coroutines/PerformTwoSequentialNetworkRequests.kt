package coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class PerformTwoSequentialNetworkRequests(
    val mockApi: MockApi,
    val viewModelScope: CoroutineScope
) {
    fun performTwoSequentialNetworkRequest(): Unit {
        viewModelScope.launch {
            val user = try {
                async {
                    mockApi.getUser()
                }.await()
            } catch (exception: Exception) {
                ""
            }
            val categories = try {
                async {
                    mockApi.getCategories()
                }
            } catch (exception: Exception) {
                emptyList<String>()
            }

        }
    }
}



