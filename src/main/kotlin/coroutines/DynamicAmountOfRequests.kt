package coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class DynamicAmountOfRequests(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {

    fun dynamicAmountOfRequests() {
        viewModelScope.launch {
            try {
                val categoryAmt = async {
                    mockApi.getCategories(6)
                }.await()
                val categories = async {
                    mockApi.getCategoryList(categoryAmt)
                }.await()
            } catch (exception: Exception) {
                ""
            }
        }
    }
}
